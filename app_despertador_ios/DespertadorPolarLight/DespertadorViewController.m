//
//  DespertadorViewController.m
//  DespertadorPolarLight
//
//  Created by Lucia Ojeda on 6/6/13.
//  Copyright (c) 2013 Lucia Ojeda. All rights reserved.
//

#import "DespertadorViewController.h"
#import "DespertadorValueObject.h"

#define PERSISTENCE_STRING_REGISTER @"registerDataDespertador"
#define NOTIFICATION_MESSAGE @"mensaje"
#define DICTIONARY_STRING_TYPE @"activityTypeIndex"
#define DICTIONARY_STRING_INDEX @"activityArrayIndex"

@interface DespertadorViewController ()

@end

@implementation DespertadorViewController

@synthesize hour, minutes, period;
@synthesize header, girl, button, footer;

NSMutableArray *arrHour;
NSMutableArray *arrMinute;
NSMutableArray *arrPeriod;

NSMutableArray *week;
NSMutableArray *selWeek;

NSArray *arrGirls;
NSArray *arrDays;

int girlSel;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSLog(@"VERSION: %@",[[UIDevice currentDevice] systemVersion]);
    
   /* if([[[UIDevice currentDevice] systemVersion] rangeOfString: @"7.0"].location != NSNotFound) {
        [hour setAlpha:1.0];
        [minutes setAlpha:1.0];
        [period setAlpha:1.0];
    }else{
        [hour setAlpha:0.7];
        [minutes setAlpha:0.7];
        [period setAlpha:0.7];
    }*/
    
    [hour  setBackgroundColor:[UIColor colorWithWhite:1.0 alpha:0.2]];
    [minutes setBackgroundColor:[UIColor colorWithWhite:1.0 alpha:0.2]];
    [period setBackgroundColor:[UIColor colorWithWhite:1.0 alpha:0.2]];
    
    //Colocar background de las vistas 'modales' en transparencia
    [[self viewGirl] setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.7f]];
    [[self viewDay] setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.7f]];
    
    // Inicializar el arreglo de horas y agregarle los valores
    arrHour = [[NSMutableArray alloc] init];
    
    for (int i=0; i<12; i++) {
        if(i < 9){
            [arrHour insertObject:[NSString stringWithFormat:@"0%d", i+1] atIndex:i];
        }else{
            [arrHour insertObject:[NSString stringWithFormat:@"%d", i+1] atIndex:i];
        }
    }
    
    // Inicializar el arreglo de minutos y agregarle los valores
    arrMinute = [[NSMutableArray alloc] init];
    
    for (int i=0; i<60; i++) {
        if(i < 10){
            [arrMinute insertObject:[NSString stringWithFormat:@"0%d", i] atIndex:i];
        }else{
            [arrMinute insertObject:[NSString stringWithFormat:@"%d", i] atIndex:i];
        }
    }
    
    // Inicializar el arreglo de periodo y agregarle los valores
    arrPeriod = [[NSMutableArray alloc] init];
    [arrPeriod insertObject:@"AM" atIndex:0];
    [arrPeriod insertObject:@"PM" atIndex:1];
    
    
    // Generar arreglo de chicas y de dias
    arrGirls = [[NSArray alloc] initWithObjects:@"Yuvanna Montalvo",
                @"Gabriela Ferrari",
                @"Andrea Matthies",
                @"Grabiela Concepción",
                @"Ivanna Vale",
                @"Hannelly Quintero", nil];
    
    arrDays = [[NSArray alloc] initWithObjects:@"Lunes",
               @"Martes",
               @"Miércoles",
               @"Jueves",
               @"Viernes",
               @"Sábado",
               @"Domingo", nil];
    
    // Seleccionar por defecto la chica 1 que es la seleccionada en el fondo de pantalla por defecto
    NSIndexPath *index = [NSIndexPath indexPathForRow:0 inSection:0];
    [[self tableViewGirls] selectRowAtIndexPath:index animated:NO scrollPosition:UITableViewScrollPositionTop];
    
    selWeek = [[NSMutableArray alloc] initWithObjects:[self monday],
               [self tuesday],
               [self wednesday],
               [self thursday],
               [self friday],
               [self saturday],
               [self sunday], nil];
    
    week = [[NSMutableArray alloc] initWithObjects:@"0", @"0", @"0", @"0", @"0", @"0", @"0", nil];
    
    [self logNotificationsInformation];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Acciones en pantalla

// Abre ventana de modal de chicas
-(IBAction)goToSelectGirl:(id)sender
{
    //Set y position to animate it
    UIView *girls = [self viewGirl];
    
    CGRect frame = [girls frame];
    frame.origin.y = 0;
    
    [UIView animateWithDuration:0.5 animations:^{
        girls.frame = frame;
    }];
    
}

// Cierra ventana modal de chicas
-(IBAction)cancelSelectGirl:(id)sender
{
    //Set y position to animate it
    UIView *girls = [self viewGirl];
    
    CGRect frame = [girls frame];
    frame.origin.y = 548;
    
    [UIView animateWithDuration:0.5 animations:^{
        girls.frame = frame;
    }];
    
}

// Abre ventana modal de dias
-(IBAction)goToSelectDay:(id)sender
{
    UIView *days = [self viewDay];
    
    CGRect frame = [days frame];
    frame.origin.y = 0;
    
    [UIView animateWithDuration:0.5 animations:^{
        days.frame = frame;
    }];

}

// Cierra ventana modal de dias
-(IBAction)cancelSelectDay:(id)sender
{
    UIView *days = [self viewDay];
    
    CGRect frame = [days frame];
    frame.origin.y = 548;
    
    [UIView animateWithDuration:0.5 animations:^{
        days.frame = frame;
    }];
    
}

// Cambiar pantalla según chica seleccionada por el usuario
-(void)changeScreen:(int) girlSelected
{
    girlSel = girlSelected;
    
    [[self girl] setImage: [UIImage imageNamed:[NSString stringWithFormat:@"bg_chica%d.jpg", girlSelected]]];
    
    if(girlSelected == 3)
    {
        girlSelected = 1;
    }
    
    

    
    [[self header] setImage: [UIImage imageNamed:[NSString stringWithFormat:@"barra_chica%d.png", girlSelected]]];
    [[self footer] setImage: [UIImage imageNamed:[NSString stringWithFormat:@"footer_chica%d.png", girlSelected]]];
    [[self button] setImage: [UIImage imageNamed:[NSString stringWithFormat:@"button_chica%d.png", girlSelected]]];
    
    
    
}

-(IBAction)acceptGirl:(id)sender
{
    [self changeScreen:([[[self tableViewGirls] indexPathForSelectedRow] row] + 1)];
    
    [self cancelSelectGirl:nil];
    
}

// Seleccionar dias y cambiar los labels con los días seleccionados por el usuario
-(void)changeDays:(NSArray *) daysSelected
{
    for(int i=0; i<[week count]; i++){
        [week replaceObjectAtIndex:i withObject:@"0"];
        
        [[selWeek objectAtIndex:i] setTextColor:[UIColor grayColor]];
    }
    
    for(NSIndexPath *path in daysSelected){
        [week replaceObjectAtIndex:[path row] withObject:@"1"];
        
        [[selWeek objectAtIndex:[path row]] setTextColor:[UIColor whiteColor]];
        
    }
    
}

-(IBAction)acceptDays:(id)sender
{
    [self changeDays:[[self tableViewDays] indexPathsForSelectedRows]];
    
    [self cancelSelectDay:nil];
}

#pragma mark - Guardar / Generar notificacion

-(IBAction)deleteAlarm:(id)sender{
    
    NSUserDefaults *preferences = [NSUserDefaults standardUserDefaults];
    [preferences setObject:nil forKey:PERSISTENCE_STRING_REGISTER];
    [preferences synchronize];

    [self deleteLocalNotificationType];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alarma Cancelada" message:@"La alarma programada fue cancelada" delegate:nil cancelButtonTitle:@"Aceptar" otherButtonTitles:nil, nil];
    
    [alert show];
}


-(IBAction)setAlarm:(id)sender
{   
    int hora = ([hour selectedRowInComponent:0] + 1);
    int minuto = [minutes selectedRowInComponent:0];
    int periodo = [period selectedRowInComponent:0];
    
    if(periodo == 1){
        hora = hora + 12;
    }
    
    if(girlSel == 0){
        girlSel = 1;
    }
    
    // Generar objeto para guardar en persistencia
    DespertadorValueObject *value = [[DespertadorValueObject alloc] initWithGirl:girlSel andHour:hora andMinutes:minuto andMeridean:periodo andDays:week];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alarma Guardada" message:@"Su alarma fue guardada con los datos seleccionados" delegate:nil cancelButtonTitle:@"Aceptar" otherButtonTitles:nil, nil];
    
    [alert show];
    
    // Guardar en persistencia el objeto de lo seleccionado
    NSUserDefaults *preferences = [NSUserDefaults standardUserDefaults];
    NSData *myEncodedRegister = [NSKeyedArchiver archivedDataWithRootObject:value];
    [preferences setObject:myEncodedRegister forKey:PERSISTENCE_STRING_REGISTER];
    [preferences synchronize];
    
    // Borrar las notificaciones
    [self deleteLocalNotificationType];
    
    // Generar notificacion en iPhone
    [self saveLocalNotificationWithHour:hora andMinutes:minuto andDays:week];
    
}

- (DespertadorValueObject *)loadRegister
{
    NSUserDefaults *preferences = [NSUserDefaults standardUserDefaults];
    NSData *myEncodedRegister = [preferences objectForKey:PERSISTENCE_STRING_REGISTER];
    
    if(myEncodedRegister) {
        DespertadorValueObject *personalData = (DespertadorValueObject *)[NSKeyedUnarchiver unarchiveObjectWithData: myEncodedRegister];
        return personalData;
    }
    
    return nil;
}

- (void)deleteLocalNotificationType
{
    UILocalNotification *actualNotification;
    NSArray *arrayNotification = [[UIApplication sharedApplication] scheduledLocalNotifications];
    for (int i=0; i<[arrayNotification count]; i++)
    {
        actualNotification = [arrayNotification objectAtIndex:i];
        [[UIApplication sharedApplication] cancelLocalNotification:actualNotification];
        actualNotification = nil;
    }
}

- (void)saveLocalNotificationWithHour:(NSInteger)horaSel andMinutes:(NSInteger)minutoSel andDays:(NSMutableArray *)days
{
    // Create data of Today Date
    NSDate *now = [NSDate date];
    NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
    NSDateComponents *todayComponents = [calendar components:( NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit ) fromDate:now];
    
    // Create data of Notification Date
    NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
    [dateComponents setDay:[todayComponents day]];
    [dateComponents setMonth:[todayComponents month]];
    [dateComponents setYear:[todayComponents year]];
    [dateComponents setHour:horaSel];
    [dateComponents setMinute:minutoSel];
	[dateComponents setSecond:0];
    
    // Create data of Notification Date
    NSDate *itemDate = [calendar dateFromComponents:dateComponents];
    
    // If time of Notification is later of "now" increase in one day
    if ([now compare:itemDate] == NSOrderedDescending)
        itemDate = [itemDate dateByAddingTimeInterval:60*60*24];
    
    UILocalNotification *localNotification;
    NSDateFormatter *timeFormat = [[NSDateFormatter alloc] init];
    [timeFormat setDateFormat:@"c"];
    
    NSDate *itemDateRecurrence = itemDate;
    
    int dayOfWeek = [[timeFormat stringFromDate:itemDateRecurrence] intValue]-2; // MOSCA!!!
    
    for (int i=0; i<[days count]; i++) {
        if ([[days objectAtIndex:dayOfWeek] isEqual:(@"1")]){
            
            // Create Notification
            localNotification = [[UILocalNotification alloc] init];
            [localNotification setFireDate:itemDateRecurrence];
            [localNotification setTimeZone:[NSTimeZone defaultTimeZone]];
            [localNotification setAlertBody:[NSString stringWithFormat:NOTIFICATION_MESSAGE]];
            [localNotification setAlertAction:@"View"];
            [localNotification setSoundName:[NSString stringWithFormat:@"sonido_chica%d_%d.mp3", girlSel, arc4random_uniform(3)]];
            [localNotification setApplicationIconBadgeNumber:1];
            [localNotification setRepeatInterval:NSWeekCalendarUnit];
            
            NSLog(@"Chica escogida %@", [localNotification soundName]);
            
            // Schedule the Notification
            [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
        }
        
        itemDateRecurrence = [itemDateRecurrence dateByAddingTimeInterval:60*60*24];
        
        dayOfWeek = dayOfWeek + 1;
        dayOfWeek = dayOfWeek % 7;
    }
}

#pragma mark - Delegate y DataSource IUPicker

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    switch ([pickerView tag]) {
        case 1:
            return [arrHour count];
            break;
        case 2:
            return [arrMinute count];
            break;
        case 3:
            return [arrPeriod count];
            break;
        default:
            return 0;
            break;
    }
    
}
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    switch ([pickerView tag]) {
        case 1:
            return [arrHour objectAtIndex:row];
            break;
        case 2:
            return [arrMinute objectAtIndex:row];
            break;
        case 3:
            return [arrPeriod objectAtIndex:row];
            break;
        default:
            return 0;
            break;
    }
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    switch ([tableView tag]) {
        case 1:
            return [arrGirls count];
            break;
        case 2:
            return [arrDays count];
        default:
            return 0;
            break;
    }
}

#pragma mark - Delegate y DataSource UITableView

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = nil;
    NSString *textLabel = nil;
    
    if([tableView tag] == 1){
        textLabel = [[arrGirls objectAtIndex:[indexPath row]] uppercaseString];
        
        cell = [tableView dequeueReusableCellWithIdentifier:@"girlName"];
        if(cell == nil)
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"girlName"];
        
        if(cell){
            // Se modifica el texto del elemento de la lista
            [[cell textLabel] setText:textLabel];
            
            // Se modifica el color del fondo para el evento de selección
            UIColor *bgSelectedColor = [[UIColor alloc] initWithRed:1.00 green:0.44 blue:0.13 alpha:1.00];
            UIView *bgSelectedColorView = [[UIView alloc] init];
            [bgSelectedColorView setBackgroundColor:bgSelectedColor];
            [cell setSelectedBackgroundView:bgSelectedColorView];
        }
    }else{
        textLabel = [[arrDays objectAtIndex:[indexPath row]] uppercaseString];
        
        cell = [tableView dequeueReusableCellWithIdentifier:@"dayName"];
        if(cell == nil)
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"dayName"];
        
        if(cell){
            // Se modifica el texto del elemento de la lista
            [[cell textLabel] setText:textLabel];
            
            // Se modifica la imagen del indicador de detalle
            UIImageView *disclosureView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"select_button.png"]];
            [cell setAccessoryView:disclosureView];
            
            // Se modifica el color del fondo para el evento de selección
            UIView *bgSelectedColorView = [[UIView alloc] init];
            [bgSelectedColorView setBackgroundColor:[UIColor clearColor]];
            [cell setSelectedBackgroundView:bgSelectedColorView];

        }
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if([tableView tag] == 2){
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];

        // Se modifica la imagen del indicador de detalle
        UIImageView *disclosureView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"select_button_sel.png"]];
        [cell setAccessoryView:disclosureView];
    }
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if([tableView tag] == 2){
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        
        // Se modifica la imagen del indicador de detalle
        UIImageView *disclosureView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"select_button.png"]];
        [cell setAccessoryView:disclosureView];
    }
}

- (void)logNotificationsInformation
{
    NSLog(@"logNotificationsInformation");
    
    UILocalNotification *actualNotification;
    
    NSDateFormatter *timeFormat = [[NSDateFormatter alloc] init];
    [timeFormat setDateFormat:@"cccc, dd/MM/yyyy hh:mm a"];
    
    NSArray *arrayNotification = [[UIApplication sharedApplication] scheduledLocalNotifications];
    for (int i=0; i<[arrayNotification count]; i++)
    {
        actualNotification = [arrayNotification objectAtIndex:i];
//        NSDictionary *actualNotificationInfo = [actualNotification userInfo];
        
        NSLog(@"------------------------------------------------");
//        NSLog(@"Notification of type %@ in position %@", [actualNotificationInfo valueForKey:DICTIONARY_STRING_TYPE], [actualNotificationInfo valueForKey:DICTIONARY_STRING_INDEX]);
        NSLog(@"%@ at %@", [actualNotification alertBody], [timeFormat stringFromDate:[actualNotification fireDate]]);
        actualNotification = nil;
    }
    NSLog(@"------------------------------------------------");
}


@end
