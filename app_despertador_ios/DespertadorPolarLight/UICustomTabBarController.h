//
//  UICustomTabBarController.h
//  DespertadorPolarLight
//
//  Created by Martin Rafael Gonzalez on 7/1/16.
//  Copyright © 2016 Lucia Ojeda. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UICustomTabBarController : UITabBarController

@end
