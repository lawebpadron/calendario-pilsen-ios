//
//  DespertadorValueObject.m
//  DespertadorPolarLight
//
//  Created by Lucia Ojeda on 6/19/13.
//  Copyright (c) 2013 Lucia Ojeda. All rights reserved.
//

#import "DespertadorValueObject.h"

@implementation DespertadorValueObject

-(id)initWithGirl:(NSInteger)index andHour:(NSInteger)hour andMinutes:(NSInteger)minutes andMeridean:(NSInteger)meridean andDays:(NSMutableArray *)days;
{
    //    @property (nonatomic) NSInteger activityIndex, activityHour, activityMinutes, activityMeridean;
    self = [super init];
	if( self ) {
        [self setActivityGirl:index];
        [self setActivityHour:hour];
        [self setActivityMinutes:minutes];
        [self setActivityMeridean:meridean];
        [self setActivityDays:days];
	}
	return self;
}

-(void)encodeWithCoder:(NSCoder *)encoder
{
    //Encode the properties of the object
    [encoder encodeInteger:[self activityGirl] forKey:@"activityGirl"];
    [encoder encodeInteger:[self activityHour] forKey:@"activityHour"];
    [encoder encodeInteger:[self activityMinutes] forKey:@"activityMinutes"];
    [encoder encodeInteger:[self activityMeridean] forKey:@"activityMeridean"];
    
    [encoder encodeObject:[self activityDays] forKey:@"activityDays"];
}

-(id)initWithCoder:(NSCoder *)decoder
{
    self = [super init];
    if (self != nil) {
        //decode the properties
        [self setActivityGirl:[decoder decodeIntegerForKey:@"activityGirl"]];
        [self setActivityHour:[decoder decodeIntegerForKey:@"activityHour"]];
        [self setActivityMinutes:[decoder decodeIntegerForKey:@"activityMinutes"]];
        [self setActivityMeridean:[decoder decodeIntegerForKey:@"activityMeridean"]];
        
        [self setActivityDays:[decoder decodeObjectForKey:@"activityDays"]];
    }
    return self;
}

@end
