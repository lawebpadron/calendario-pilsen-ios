//
//  AgeSelectionViewController.h
//  DespertadorPolarLight
//
//  Created by Martin Rafael Gonzalez on 7/1/16.
//  Copyright © 2016 Lucia Ojeda. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AgeSelectionViewController : UIViewController <UIPickerViewDataSource, UIPickerViewDelegate>

@property IBOutlet UIPickerView * days;
@property IBOutlet UIPickerView * month;
@property IBOutlet UIPickerView * years;


-(IBAction)checkAge:(id)sender;


@end
