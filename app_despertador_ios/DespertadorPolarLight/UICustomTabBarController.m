//
//  UICustomTabBarController.m
//  DespertadorPolarLight
//
//  Created by Martin Rafael Gonzalez on 7/1/16.
//  Copyright © 2016 Lucia Ojeda. All rights reserved.
//

#import "UICustomTabBarController.h"

@interface UICustomTabBarController ()

@end

@implementation UICustomTabBarController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    UITabBar *tabBar = self.tabBar;
    [tabBar setBarStyle:UIBarStyleDefault];
    [tabBar setBackgroundColor:[UIColor colorWithRed:0 green:0.627 blue:0.729 alpha:1]];
    
    
    NSLog(@"PEPA");
    
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
