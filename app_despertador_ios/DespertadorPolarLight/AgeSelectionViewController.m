//
//  AgeSelectionViewController.m
//  DespertadorPolarLight
//
//  Created by Martin Rafael Gonzalez on 7/1/16.
//  Copyright © 2016 Lucia Ojeda. All rights reserved.
//

#import "AgeSelectionViewController.h"

@interface AgeSelectionViewController ()

@end




@implementation AgeSelectionViewController

@synthesize days;
@synthesize month;
@synthesize years;


NSMutableArray *arrDays;
NSMutableArray *arrMonth;
NSMutableArray *arrYears;


- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    days = [[UIPickerView alloc] init];
    month = [[UIPickerView alloc] init];
    years = [[UIPickerView alloc] init];
    
    
    [days setDelegate:self];
    [days setDataSource:self];
    [month setDelegate:self];
    [month setDataSource:self];
    [years setDelegate:self];
    [years setDataSource:self];
    
    
    
  
    // Inicializar el arreglo de dias
    arrDays = [[NSMutableArray alloc] init];
    
    for (int i=0; i<31; i++) {
        if(i < 9){
            [arrDays insertObject:[NSString stringWithFormat:@"0%d", i+1] atIndex:i];
        }else{
            [arrDays insertObject:[NSString stringWithFormat:@"%d", i+1] atIndex:i];
        }
    }
    
    //Inicializar el arreglo de meses
    arrMonth = [[NSMutableArray alloc] init];
    
    for (int i=0; i<12; i++) {
        if(i < 9){
            [arrMonth insertObject:[NSString stringWithFormat:@"0%d", i+1] atIndex:i];
        }else{
            [arrMonth insertObject:[NSString stringWithFormat:@"%d", i+1] atIndex:i];
            
        }
    }
    
    
    //Inicializar el arreglo de anios
    arrYears = [[NSMutableArray alloc] init];
    
    NSDate *currentDate = [NSDate date];
    NSCalendar* calendar = [NSCalendar currentCalendar];
    NSDateComponents* components = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:currentDate]; // Get necessary date components
    
    int year = (int)[components year] ; // gives you year
    int firstYear = year - 70;
    
    for (int i=0; i<70; i++) {
        
        [arrYears insertObject:[NSString stringWithFormat:@"%d", firstYear] atIndex:i];
        firstYear++;
            
    }
    
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



//chequeo si soy mayor de edad
-(IBAction)checkAge:(id)sender{

    NSDate *currentDate = [NSDate date];
    NSCalendar* calendar = [NSCalendar currentCalendar];
    NSDateComponents* components = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:currentDate]; // Get necessary date components
    
    int year = (int)[components year];
    int mon = (int)[components month];
    int day = (int)[components day];
    
    int selectedYear = [[arrYears objectAtIndex:[years selectedRowInComponent:0]] intValue];
    int selectedMonth = ([month selectedRowInComponent:1]+1);
    int selectedDay =  ([days selectedRowInComponent:1]+1);
    
    
    
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Calendario Mensaje" message:@"Debes ser mayor de edad para poder utilizar la aplicación" delegate:self cancelButtonTitle:@"Aceptar" otherButtonTitles:nil];
    
    
    NSLog(@"Datos %d %d %d",selectedYear, year, selectedMonth);
    
    //Si mi anio es mayor al minimo de 18
    if ((year - selectedYear) < 17) {
        [alert show];
    }
    
    
    //Si mi anio es exactamente 17 y no ha pasado mi mes de cumple
    else if((year - selectedYear == 17) &&  (selectedMonth < mon)){
        [alert show];
    }
    
    //Si mi anio y mi mes son exactamente los de cumplir 18 pero no ha pasado mi dia
    else if((year - selectedYear == 17) &&  (selectedMonth == mon) && (selectedDay<day)){
        [alert show];
    
    }else{
        //[self performSegueWithIdentifier:@"segueAgeToPick" sender:self];
    
    }
    
}




#pragma mark - Delegate y DataSource IUPicker
-(NSInteger) numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}


- (NSAttributedString *)pickerView:(UIPickerView *)pickerView attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    
    NSString * title;
    
    switch ([pickerView tag]) {
        case 1:
            title = [arrDays objectAtIndex:row];
            break;
        case 2:
            title = [arrMonth objectAtIndex:row];
            break;
        case 3:
            title = [arrYears objectAtIndex:row];
            break;
        default:
            return 0;
            break;
    }
    

    NSAttributedString *attString = [[NSAttributedString alloc] initWithString:title attributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    return attString;
    
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    switch ([pickerView tag]) {
        case 1:
            return [arrDays count];
            break;
        case 2:
            return [arrMonth count];
            break;
        case 3:
            return [arrYears count];
            break;
        default:
            return 0;
            break;
    }
    
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    switch ([pickerView tag]) {
        case 1:
            return [arrDays objectAtIndex:row];
            break;
        case 2:
            return [arrMonth objectAtIndex:row];
            break;
        case 3:
            return [arrYears objectAtIndex:row];
            break;
        default:
            return 0;
            break;
    }
    
}





@end
