//
//  DespertadorAppDelegate.h
//  DespertadorPolarLight
//
//  Created by Lucia Ojeda on 6/6/13.
//  Copyright (c) 2013 Lucia Ojeda. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DespertadorAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
