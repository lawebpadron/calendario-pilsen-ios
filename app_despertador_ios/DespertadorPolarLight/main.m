//
//  main.m
//  DespertadorPolarLight
//
//  Created by Lucia Ojeda on 6/6/13.
//  Copyright (c) 2013 Lucia Ojeda. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "DespertadorAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([DespertadorAppDelegate class]));
    }
}
