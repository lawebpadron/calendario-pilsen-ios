//
//  DespertadorViewController.h
//  DespertadorPolarLight
//
//  Created by Lucia Ojeda on 6/6/13.
//  Copyright (c) 2013 Lucia Ojeda. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DespertadorViewController : UIViewController <UIPickerViewDataSource, UIPickerViewDelegate, UITableViewDelegate>

@property IBOutlet UIView *viewGirl;
@property IBOutlet UIView *viewDay;

@property IBOutlet UIPickerView *hour;
@property IBOutlet UIPickerView *minutes;
@property IBOutlet UIPickerView *period;

@property IBOutlet UITableView *tableViewGirls;
@property IBOutlet UITableView *tableViewDays;

@property IBOutlet UIImageView *header;
@property IBOutlet UIImageView *girl;
@property IBOutlet UIImageView *button;
@property IBOutlet UIImageView *footer;

@property IBOutlet UILabel *monday;
@property IBOutlet UILabel *tuesday;
@property IBOutlet UILabel *wednesday;
@property IBOutlet UILabel *thursday;
@property IBOutlet UILabel *friday;
@property IBOutlet UILabel *saturday;
@property IBOutlet UILabel *sunday;

-(IBAction)goToSelectGirl:(id)sender;
-(IBAction)cancelSelectGirl:(id)sender;
-(IBAction)goToSelectDay:(id)sender;
-(IBAction)cancelSelectDay:(id)sender;

-(IBAction)acceptGirl:(id)sender;
-(IBAction)acceptDays:(id)sender;

-(IBAction)setAlarm:(id)sender;
-(IBAction)deleteAlarm:(id)sender;

@end
