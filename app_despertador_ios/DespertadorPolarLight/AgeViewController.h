//
//  AgeViewController.h
//  DespertadorPolarLight
//
//  Created by Carolina Chang on 10/9/13.
//  Copyright (c) 2013 Lucia Ojeda. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AgeViewController : UIViewController

-(IBAction)agecontroller:(UIButton *)sender;

@end
