//
//  AgeViewController.m
//  DespertadorPolarLight
//
//  Created by Carolina Chang on 10/9/13.
//  Copyright (c) 2013 Lucia Ojeda. All rights reserved.
//

#import "AgeViewController.h"

@interface AgeViewController ()

@end

@implementation AgeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    NSUserDefaults *preferences = [NSUserDefaults standardUserDefaults];
    
    NSLog(@"VALORRRR %d ", [preferences boolForKey:@"MayorEdad"]);
    if([preferences boolForKey:@"MayorEdad"]){
        [self performSegueWithIdentifier:@"segueAgeToAlarm" sender:self];

    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)agecontroller:(UIButton *)sender{
    
    //Si dije que si
    if ([sender tag] == 1) {
        [self performSegueWithIdentifier:@"segueAgeToAlarm" sender:self];
        
        //Guardar en persistencia
        NSUserDefaults *preferences = [NSUserDefaults standardUserDefaults];
        [preferences setBool:YES forKey:@"MayorEdad"];
        [preferences synchronize];
        NSLog(@"HEEEY %d", [preferences boolForKey:@"MayorEdad"]);
        
    }else{
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Mensaje" message:@"La aplicación Despertador Chicas Polar Pilsen es solo para mayores de edad" delegate:self cancelButtonTitle:@"Aceptar" otherButtonTitles: nil];
        [alert show];
    }

}

@end
