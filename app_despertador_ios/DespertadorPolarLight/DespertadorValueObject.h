//
//  DespertadorValueObject.h
//  DespertadorPolarLight
//
//  Created by Lucia Ojeda on 6/19/13.
//  Copyright (c) 2013 Lucia Ojeda. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DespertadorValueObject : NSObject

@property (nonatomic) NSInteger activityGirl, activityHour, activityMinutes, activityMeridean;
@property NSMutableArray *activityDays;

-(id)initWithGirl:(NSInteger)index andHour:(NSInteger)hour andMinutes:(NSInteger)minutes andMeridean:(NSInteger)meridean andDays:(NSMutableArray *)days;

@end
